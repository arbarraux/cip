#!/usr/bin/env python3

import cv2 as cv
import numpy as np
import argparse


def show_image(image_to_be_printed, window_name):
    cv.imshow(window_name, image_to_be_printed)
    cv.waitKey(0)
    cv.destroyAllWindows()


def average(image, j_min, j_max, i_min, i_max):
    total_sum = np.zeros((3), dtype="int")
    number_of_case = 0
    for i in range(i_min, i_max, 1):
        for j in range(j_min, j_max, 1):
            try:
                total_sum[0] += image[i, j, 0]
                total_sum[1] += image[i, j, 1]
                total_sum[2] += image[i, j, 2]
                number_of_case += 1
            except IndexError:
                print("warnig", i, j)
                pass

    for i in range(3):
        total_sum[i] = total_sum[i] // number_of_case
    return total_sum


def print_image(image):
    for line in image:
        for box in line:
            print(f"\033[48;2;{box[2]};{box[1]};{box[0]}m ", end="\033[49m")
        print()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("image_path")
    parser.add_argument("--dim")
    args = parser.parse_args()
    if args.dim:
        width, height = map(int, args.dim.split("X"))
    else:
        width, height = 80, 24

    image = cv.imread(args.image_path)
    image_height, image_width, _ = image.shape
    width_ratio = image_width // width
    height_ratio = image_height // height

    output_image = np.zeros((height, width, 3))
    for i, line in enumerate(output_image):
        for j, cols in enumerate(line):
            output_image[i, j] = average(
                image,
                j * width_ratio,
                (j + 1) * width_ratio,
                i * height_ratio,
                (i + 1) * height_ratio,
            )
    output_image = output_image.astype("uint8")
    print_image(output_image)


#
#
#                              -*+:
#                              **###*-
#                              *#-#**%-   :+*+:.
#                              :#%.+-=#. :+##%*.
#                               -@*#:.%:.-**:+%=
#                               .##-+.=*:=%:=-%#
#                               .*#-@=*@=*%+:*@#
#                               .=%%%-@#%%-*=.@-
#                 -++++##+#@#-.  .+%==#:##*-**#.  ...
#              .=#@#*--++-%%%%%#- *@-=--.-==:+*#######+:
#           .:+#@@#=*#:=-*%:+=:=#**%.-:-:+.-=--:=*##%%@#:
#           :#@%+-.:+%%##%=:=*-:=%#*++=-.*=*-=+===+###**-
#           -%=-=**+=---==:::+#=-:.:@=@=##*===---*##%%#+:
#           :%%%#%@%%***##%%%%%#*=#@@=@#--#@*+=:-::.
#            -==+--=*#*-=##***##@#%%%=%+*===:*:=#%#=:.
#                             .:=*=:=%@%::-:+*-.=-+@#*-..
#                           .:=+=-.=@@+=+.=#*=%**+:+-**=:
#                          :=+=:-=.+@:%.#:+#@@@*-*--=:*@%+:
#                        ..--.-+=: *%-*#*:%#:+%@@%=-=::=##+:
#                       .:::-==-.  *#:::::@+. .:=+*#:-+==++=
#                      .::-:::.    *#.-+-:%+.   ..-##**%#*+=
#                     .-=----..    -@==::.*#.      -**+*#*=:
#                    .:-:.-+:      .@*-=-*#*
#                   .::::==:.       ####%#+-
#                  .::.::-:.        -**##-.
#                  .=-:--=..         ....
#                 .:=...=-.
#                 .==.:-=:
#                 :+===--.
#                 :+=--=:.
#                .:--=-::.
#
#

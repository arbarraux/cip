# CIP

## Description

Cli Image Printer is a program to print image in the terminal.

## Dependencies

You'll need _open cv 2_, _numpy_, _argparse_.

## Utilisation

The program take one argument wich is the name of the image. You can add the dimensions of the output image wich are by default **80X24**.

Exemple:
`python3 cip.py image.png --dim=100X30`

## Contact

If you experience any issue, please let me know that.
